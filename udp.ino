/*
  WiFi UDP Send and Receive String

 This sketch wait an UDP packet on localPort using a WiFi shield.
 When a packet is received an Acknowledge packet is sent to the client on port remotePort

 Circuit:
 * WiFi shield attached

 created 30 December 2012
 by dlf (Metodo2 srl)

 */
#include "ESP8266WiFi.h"
#include <string>
#include "ESP8266HTTPClient.h"
#include <DHT.h>
#include <cstdlib>

int status = WL_IDLE_STATUS;
char ssid[] = "BusCBA"; //  your network SSID (name)
char pass[] = "wojtek123";    // your network password (use for WPA, or use as key for WEP)
int keyIndex = 0; 
float temperature = -5000; // if it reaches that it means were all dead xD
float humidity = -5000; // too big ;-)
float tempFluct = 1;
DHT dht(2, DHT22);
HTTPClient client;

void setup() {
   dht.begin();
}

void loop() {
  connectWifi();
  if (sendNewData()) {
    sendRequest(temperature, humidity);
  }
}

void connectWifi() {
 while (status != WL_CONNECTED) {
  status = WiFi.begin(ssid, pass);
  delay(10000);
  status = WiFi.status();
 }
 WiFi.disconnect();
 status = WL_IDLE_STATUS;
 delay(600000);  
}

void sendRequest(float& t, float& h) {

String request;
String temp = String(t);
String humid = String(h);
request += "http://vps266604.ovh.net:3000/sendWeather?shakira=yes&";
request += "temperature=" + temp + "&";
request += "humidity=" + humid;

client.begin(request);
client.GET();
client.end();
}

bool sendNewData() {
  float tempTemperature = dht.readTemperature();
  humidity = dht.readHumidity();

  // Read temperature as Celsius (the default)
  if (isnan(humidity) || isnan(tempTemperature))
    return false;
  if (abs(tempTemperature - temperature) > 1) {
    temperature = tempTemperature;
    return true;
  }
  return false;
}


